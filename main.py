import requests

API_KEY = ''
UNITS = 'metric'
MODE = 'json'
LANG = 'ru'


def get_weather(lat, lon):
    url = 'https://api.openweathermap.org/data/2.5/weather?appid=%s&lat=%s&lon=%s&units=%s&lang=%s&mode=%s'
    address = url % (API_KEY, lat, lon, UNITS, LANG, MODE)
    response = requests.get(address)
    if response.status_code == 200:
        data = response.json()
        print(data['weather'][0]['description'])
    elif response.status_code == 401:
        print('Ключ авторизации протух')
    elif response.status_code == 400:
        print('Что-то не так с запросом')
    else:
        print('Что-то пошло не так')


get_weather(52.5508, 104.4556)
